"""
    Copyright (C) 2019  Y. Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
"""
import warnings
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


warnings.filterwarnings("ignore", category=UserWarning)


class FriendGraph:
    """
    Friend/Family graph - assignment1
    """
    def __init__(self, graph_participants):
        self.graph_participants = graph_participants
        self.graph = nx.Graph()
        self.add_weights()
        self.draw_good_bad_relations()
        self.print_out_graph_content_information()

    def add_weights(self):
        """
        added weighted edges to graph which also adds nodes
        """
        weights = []
        for key1, value1 in self.graph_participants.items():
            for key2, value2 in value1.items():
                for value in value2:
                    weights.append((key1, value, key2))
        self.graph.add_weighted_edges_from(weights)

    def draw_good_bad_relations(self):
        """
        weighted version of graph where +1 weighted edges indicate "good" relations and -1 weighted
        edges indicate "bad" relations
        """
        good_relations = [(u, v) for (u, v, d) in self.graph.edges(data=True) if d['weight'] > 1]
        bad_relations = [(u, v) for (u, v, d) in self.graph.edges(data=True) if d['weight'] <= -1]

        pos = nx.spring_layout(self.graph)
        nx.draw_networkx_nodes(self.graph, pos, node_size=300, node_color='tan', node_shape='o')

        nx.draw_networkx_edges(self.graph, pos, edgelist=good_relations,
                               width=1.7, edge_color='dimgray')
        nx.draw_networkx_edges(self.graph, pos, edgelist=bad_relations,
                               width=1.7, alpha=0.5, edge_color='red', style='dashed')

        nx.draw_networkx_labels(self.graph, pos, font_size=9, alpha=1.0)
        plt.axis('off')
        plt.show()

    def print_out_graph_content_information(self):
        """
        prints out min, max and average degree
        prints out shortest path between two nodes
        prints out the cycles in the network
        """
        print('Question 1:')
        max_degree = max(dict(self.graph.degree()).items(), key=lambda x: x[1])
        print('node name that has max degree of network and its degree :', max_degree)

        degree_sequence = [d for n, d in self.graph.degree]

        print('min degree of network:', min(degree_sequence))
        print('average degree of network:', np.mean(degree_sequence))
        print('Question 2:')
        shortest_path = nx.shortest_path(self.graph, source='Candan', target='Jale')
        print('shortest path between node-Candan and node-Jale:', shortest_path)
        print('Question 3:')
        cycle = nx.find_cycle(self.graph)
        print('cycles in network: ', cycle)


    def save_graph(self):
        """
        used the pickle library to save networkx graph
        """
        nx.write_gpickle(self.graph, 'network.bin')


def main():
    """
    nested dictionary which contains: node, edge and weight information
    """
    names = {'Yagmur': {10: ['Lema', 'Mustafa', 'Bulut'], -7: ['Melek', 'Jale'],
                        6: ['Caney', 'Turhan', 'Ulas'],
                        1.5: ['Salime', 'Rasi'], 3: ['Dodi', 'Memo'],
                        -9: ['Ilmiye', 'Candan', 'Selma', 'Bahar'],
                        -4: ['Bahar', 'Ozu', 'Hakan', 'Nimet', 'Suat'],
                        2: ['Aysen', 'Veli', 'Ali', 'Sulo', 'Gorkem', 'Alhadi']},
             'Lema': {10: ['Mustafa', 'Bulut', 'Melek'], -3: ['Ilmiye'], -5: ['Caney', 'Salime']},
             'Mustafa': {10: ['Bulut', 'Melek', 'Ilmiye'], 2: ['Turhan', 'Salime'], -7: ['Selma']},
             'Bulut': {10: ['Melek', 'Ilmiye'], -5: ['Caney', 'Turhan'],
                       2: ['Ulas', 'Dodi', 'Memo']},
             'Melek': {5: ['Caney', 'Turhan']}, 'Ilmiye': {}, 'Jale': {2: ['Caney']},
             'Caney': {10: ['Turhan'], 4: ['Ulas', 'Dodi', 'Memo'],
                       -7: ['Aysen', 'Ali', 'Alhadi']}, 'Turhan': {},
             'Ulas': {9: ['Dodi', 'Memo', 'Salime']},
             'Dodi': {9: ['Memo', 'Salime'], -9: ['Selma'],
                      3: ['Aysen', 'Veli']}, 'Memo': {5: ['Aysen']}, 'Salime': {},
             'Selma': {}, 'Bahar': {4: ['Ozu', 'Rasi']}, 'Ozu': {}, 'Rasi': {}, 'Nimet': {},
             'Aysen': {7: ['Veli', 'Ali', 'Sulo'], -3: ['Gorkem', 'Hakan', 'Suat', 'Alhadi']},
             'Veli': {9: ['Ali', 'Sulo'], -8: ['Gorkem'],
                      2: ['Hakan', 'Suat', 'Alhadi']},
             'Ali': {2: ['Sulo', 'Suat', 'Alhadi']}, 'Sulo': {},
             'Gorkem': {}, 'Hakan': {}, 'Suat': {}, 'Candan': {},
             'Alhadi': {}}
    FriendGraph(names)


if __name__ == '__main__':
    main()
