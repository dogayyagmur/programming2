"""
    Copyright (C) 2019  Y. Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
"""
import io
import argparse
import os
import time, queue
import multiprocessing as mp
from multiprocessing.managers import BaseManager
from Bio import Entrez


class Assignment3:
    """
    Assignment 3: Download and analyze the NCBI articles from different "remote" clients
    Run script LOCALLY: The program needs to run from the commandline and requires positional arguments.
    input command:
    python3 assignment3.py --hosts "127.0.0.1" -n 2 -a 5 32306032
    Run script REMOTELY: The program needs to run from the commandline and requires positional arguments.
    input command:
    python3 assignment3.py -c -p 50000 --ip "192.168.2.90" -n 2 32306032
    """
    POISONPILL = "MEMENTOMORI"
    ERROR = "DOH"
    AUTHKEY = b'whathasitgotinitspocketsesss?'

    def __init__(self, server, ip, portnum):
        Entrez.api_key = '39ed0711659c7024d85225d996bca0c58708'
        Entrez.email = 'dogayyagmur@gmail.com'
        self.ref_ids = None
        self.ip = ip
        self.portnum = portnum
        self.manager = self.make_server_manager() if server else self.make_client_manager()

    def set_ref_ids(self, pm_id, n):
        self.ref_ids = self.query_article(idn=pm_id)[:n]

    def make_server_manager(self):
        """ Create a manager for the server, listening on the given port.
            Return a manager object with get_job_q and get_result_q methods.
        """
        job_q = queue.Queue()
        result_q = queue.Queue()

        class QueueManager(BaseManager):
            pass

        QueueManager.register('get_job_q', callable=lambda: job_q)
        QueueManager.register('get_result_q', callable=lambda: result_q)

        manager = QueueManager(address=('', self.portnum), authkey=Assignment3.AUTHKEY)
        manager.start()
        print('Server started at port %s' % self.portnum)
        return manager

    def runserver(self, fn, data):
        """
        Start a shared manager server and access its queues,
        Tell the client process no more data will be forthcoming,
        Sleep a bit before shutting down the server - to give clients time to,
        Realize the job queue is empty and exit in an orderly way
        And tell the client process no more data will be forthcoming
        """

        shared_job_q = self.manager.get_job_q()
        shared_result_q = self.manager.get_result_q()

        if not data:
            print("Gimme something to do here!")
            return

        print("Sending data!")
        for d in data:
            shared_job_q.put({'fn': fn, 'arg': d})

        time.sleep(10)

        results = []
        while True:
            try:
                result = shared_result_q.get_nowait()
                Assignment3.writing_articles(result)
                results.append(result)
                if len(results) == len(data):
                    print("Got all results!")
                    break
            except queue.Empty:
                time.sleep(1)
                continue
        print("Time to kill some peons!")
        shared_job_q.put(Assignment3.POISONPILL)
        time.sleep(5)
        print("Aaaaaand we're done for the server!")
        self.manager.shutdown()

    def make_client_manager(self):
        """ Create a manager for a client. This manager connects to a server on the
            given address and exposes the get_job_q and get_result_q methods for
            accessing the shared queues from the server.
            Return a manager object.
        """

        class ServerQueueManager(BaseManager):
            pass

        ServerQueueManager.register('get_job_q')
        ServerQueueManager.register('get_result_q')

        manager = ServerQueueManager(address=(self.ip, self.portnum), authkey=Assignment3.AUTHKEY)
        manager.connect()

        print('Client connected to %s:%s' % (self.ip, self.portnum))
        return manager

    @staticmethod
    def query_article(idn):
        """
        search for an article with id number,
        and find the pmid of referances for finding related articles
        """
        handle = Entrez.efetch(db='pubmed', id=idn, rettype='xml', retmode='text')
        records = Entrez.read(handle)
        idnumber = ''
        for record in records["PubmedArticle"]:
            idnumber = record["MedlineCitation"]["PMID"]
        handle = Entrez.elink(db='pubmed', id=idn)
        record = Entrez.read(handle)
        refs = [link["Id"] for link in record[0]["LinkSetDb"][0]["Link"]]
        handle.close()
        refs.remove(idnumber)
        refs = list(dict.fromkeys(refs))
        return refs

    @staticmethod
    def download_xml_file(ref_id):
        """
        get related articles in xml format
        """

        handle = Entrez.efetch(db='pubmed', id=ref_id, retmode='xml')
        return handle.read()

    def runclient(self, num_processes):
        job_q = self.manager.get_job_q()
        result_q = self.manager.get_result_q()
        Assignment3.run_workers(job_q, result_q, num_processes)

    @staticmethod
    def run_workers(job_q, result_q, num_processes):
        processes = []
        for p in range(num_processes):
            temP = mp.Process(target=Assignment3.peon, args=(job_q, result_q))
            processes.append(temP)
            temP.start()
        print("Started %s workers!" % len(processes))
        for temP in processes:
            temP.join()

    @staticmethod
    def peon(job_q, result_q):
        my_name = mp.current_process().name
        while True:
            try:
                job = job_q.get_nowait()
                if job == Assignment3.POISONPILL:
                    job_q.put(Assignment3.POISONPILL)
                    print("Aaaaaaargh", my_name)
                    return
                else:
                    try:
                        result = job['fn'](job['arg'])
                        print("Peon %s Workwork on %s!" % (my_name, job['arg']))
                        result_q.put({'job': job, 'result': result})
                    except NameError:
                        print("Can't find yer fun Bob!")
                        result_q.put({'job': job, 'result': Assignment3.ERROR})

            except queue.Empty:
                print("sleepytime for", my_name)
                time.sleep(1)

    @staticmethod
    def writing_articles(result):
        """
        download articles in xml format into a directory
        """
        with io.open("output/{}".format(str(result['job']['arg']) + '.xml'), "w",
                     encoding="utf-8") as pubmed_id_xml_file:
            pubmed_id_xml_file.write(result['result'])


class AppParser:
    """
    Gives arguments and handles the input of the arguments.
    :return: All given arguments as a list, even single values.
    """

    def __init__(self):
        """
        argument flags and names are being set
        """
        self.parser = argparse.ArgumentParser()
        self.parser.set_defaults(children=1, port=50000, hosts="127.0.0.1", ip="")
        self.arguments = self.input_arguments()

    def input_arguments(self):
        """
        sets the arguments
        :return: arguments
        """
        self.parser.add_argument("-n", "--children", required=False, type=int,
                                 help="Specify the amount of cores to use per child")
        self.parser.add_argument("-a", "--num", required=False, type=int,
                                 help="Specify the article number that need to be searched")
        self.parser.add_argument("-p", "--port", required=False, type=int,
                                 help="Specify the server port number, defaults to 50000")
        self.parser.add_argument("--ip", required=False, type=str,
                                 help="the ip address")
        self.parser.add_argument("-c", "--client", action="store_true",
                                 help="Runs the application as client")
        self.parser.add_argument("--hosts", required=False,
                                 help="Number of hosts", type=str)
        self.parser.add_argument("-o", nargs=1,
                                 help="Specify an output directory,"
                                      "if not is specified an output directory will "
                                      "be made at the position form where the script is run.",
                                 type=AppParser.directory_type)
        self.parser.add_argument("pmid",
                            help="Fill in the pubmed id you are looking for.",
                            type=int)
        return self.parser.parse_args()

    @staticmethod
    def directory_type(direc):
        """
        create output folder if not exists
        """
        cwd = os.getcwd()
        directory_exists = os.path.isdir(os.path.join(cwd, direc)) or os.path.isdir(direc)
        if not directory_exists:
            os.mkdir(os.path.join(cwd, direc))

    def __getattr__(self, item):
        """
        get method for arguments
        :param item: argument name
        :return: argument value
        """
        return self.arguments.__getattribute__(item)


def run_client(args):
    client = Assignment3(False, args.ip, args.port)
    # time.sleep(1)
    client_proc = mp.Process(target=client.runclient, args=(args.children,))
    client_proc.start()
    return client_proc


def main():
    args = AppParser()
    print(type, (args.pmid))
    if args.client:
        client_proc = run_client(args)
    else:
        server = Assignment3(True, '', args.port)
        server.set_ref_ids(args.pmid, args.num)
        server_proc = mp.Process(target=server.runserver, args=(Assignment3.download_xml_file, server.ref_ids))
        server_proc.start()
        time.sleep(1)
    if len(args.hosts.split(" ")) == 1:
        client_proc = run_client(args)
    if not args.client:
        server_proc.join()
    else:
        client_proc.join()


if __name__ == "__main__":
    main()
