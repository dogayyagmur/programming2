"""
    Copyright (C) 2019  Y. Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
"""
import os.path
import io
import multiprocessing as mp
import argparse
from Bio import Entrez


class Assignment2:
    """
    query NCBI Pubmed for an article of your own choice and to parse
    the resulting file (in XML format) to finr 9 more articles linked
    to it and to get used to using the Python multiprocessing package
    """
    def __init__(self):
        self.input_arguments = self.input_arguments_query_article()
        self.ref_ids = self.query_article(self.input_arguments.i[0])[0:10]
        if self.input_arguments.o is None:
            self.create_output_directory_if_not_exists()
        self.core_count = self.handle_maximum_processor_count(self.input_arguments.n[0])
        self.divide_list_among_cpu_cores()

    def divide_list_among_cpu_cores(self):
        """
        multiprocessing
        """
        jobs = []
        step_size = round(len(self.ref_ids) / self.core_count)
        for step in range(0, len(self.ref_ids), step_size):
            if (step + step_size) >= len(self.ref_ids):
                process = mp.Process(target=self.downloading_xml_files,
                                     args=(self.ref_ids[step:None],))
                jobs.append(process)
                process.start()
            else:
                process = mp.Process(target=self.downloading_xml_files,
                                     args=(self.ref_ids[step:step + step_size],))
                jobs.append(process)
                process.start()
        for job in jobs:
            job.join()

    @staticmethod
    def handle_maximum_processor_count(number_of_given_processors):
        """
        handle maximum processor count
        """
        if number_of_given_processors > 9:
            number_of_given_processors = 9
        return number_of_given_processors

    @staticmethod
    def input_arguments_query_article():
        """
        Gives arguments and handles the input of the arguments.
        :return: All given arguments as a list, even single values.
        """
        parser = argparse.ArgumentParser()
        parser.add_argument("-n", nargs=1,
                            help="Specify amount of threads"
                                 "to be used by the script, the maximum is 9.",
                            type=int,
                            default=[1])
        parser.add_argument("-i", nargs=1,
                            help="Fill in the pubmed id you are looking for.",
                            type=int, required=True)
        parser.add_argument("-o", nargs=1,
                            help="Specify an output directory,"
                                 "if not is specified an output directory will "
                                 "be made at the position form where the script is run.", type=str)

        collected_arguments = parser.parse_args()
        return collected_arguments

    @staticmethod
    def query_article(idn):
        """
        search for an article with id number,
        and find the pmid of referances for finding related articles
        """
        handle = Entrez.efetch(db='pubmed', id=idn, rettype='xml', retmode='text')
        records = Entrez.read(handle)
        idnumber = ''
        for record in records["PubmedArticle"]:
            idnumber = record["MedlineCitation"]["PMID"]
        handle = Entrez.elink(db='pubmed', id=idn)
        record = Entrez.read(handle)
        refs = [link["Id"] for link in record[0]["LinkSetDb"][0]["Link"]]
        handle.close()
        refs.remove(idnumber)
        refs = list(dict.fromkeys(refs))
        return refs

    @staticmethod
    def create_output_directory_if_not_exists():
        """
        create output folder if not exists
        """
        if os.path.exists("./output/"):
            pass
        else:
            os.mkdir("./output/")

    @staticmethod
    def downloading_xml_files(ref_ids):
        """
        downloading related articles in a folder
        """
        for ref_id in ref_ids:
            handle = Entrez.efetch(db='pubmed', id=ref_id, retmode='xml')
            records = handle.read()

            with io.open("output/{}".format(str(ref_id) + ".xml"), "w",
                         encoding="utf-8") as pubmed_id_xml_file:
                pubmed_id_xml_file.write(records)


def main():
    """
    api_key, email
    """
    Entrez.api_key = '39ed0711659c7024d85225d996bca0c58708'
    Entrez.email = 'dogayyagmur@gmail.com'
    Assignment2()


if __name__ == '__main__':
    main()
